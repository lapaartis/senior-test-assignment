<?php


session_start();

/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| Require composer autoload into the script here to that we don't have to
| worry about manual loading any of classes.
|
*/
require __DIR__ . '/../vendor/autoload.php';

/*
|--------------------------------------------------------------------------
| Register The App features
|--------------------------------------------------------------------------
|
*/
require __DIR__ . '/../app/Libs/autoload.php';

