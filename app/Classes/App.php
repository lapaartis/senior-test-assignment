<?php


namespace App\Classes;


class App
{

    /**
     * Collect app config.
     *
     * @var array
     */
    protected static $registry = [];

    /**
     * Create a new class instance.
     *
     * @param  array  $array
     * @return void
     */
    public function __construct(array $array)
    {
        static::$registry = $array;
    }

    /**
     * Bind a new array key
     *
     * @param  string  $key
     * @param  string  $value
     * @return void
     */
    public static function bind($key, $value)
    {
        static::$registry[$key] = $value;
    }

    /**
     * Get a array of key
     *
     * @param  string  $key
     * @expectedException \Exception
     * @return array
     */
    public static function get($key)
    {
        if (! array_key_exists($key, static::$registry)) {
            throw new \Exception("No {$key} is found in the container.");
        }

        return static::$registry[$key];
    }
}