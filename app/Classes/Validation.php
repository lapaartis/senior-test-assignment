<?php
/**
 * Created by PhpStorm.
 * User: LEAF
 * Date: 02.11.2018
 * Time: 04:42
 */

namespace App\Classes;

use Illuminate\Database\Capsule\Manager as DB;

class Validation
{
    /**
     * @var array $patterns
     */
    public $patterns = array(
        'integer' => '-?[0-9]+',
        'float' => '-?[0-9\.,]+',
        'string' => '[\p{L}0-9\s-.,;:!"%&()?+\'°#\/@]+',
        'currency' => '-?\d+(\.\d{2})?',
//        'currency' => '^(-?([0-9]*\.?\d{2}?)$)',
    );


    /**
     * @var array $errors
     */
    public $errors = array();

    /**
     * Name of value
     *
     * @param string $name
     * @return this
     */
    public function name($name)
    {

        $this->name = $name;
        return $this;

    }

    /**
     * Value
     *
     * @param mixed $value
     * @return this
     */
    public function value($value)
    {

        $this->value = $value;
        return $this;

    }

    /**
     * Required field
     *
     * @return this
     */
    public function required()
    {

        if ((isset($this->file) && $this->file['error'] == 4) || ($this->value == '' || gettype($this->value) === 'NULL')) {
            $this->errors[$this->name][] = validation_trans('required', ['attribute' => $this->name]);
        }
        return $this;

    }

    /**
     * Minimum length of the field value
     *
     * @param int $min
     * @return this
     */
    public function min($length)
    {
        $is_number = preg_match('/^(' . $this->patterns['float'] . ')$/u', $this->value);

        if (is_string($this->value) && !$is_number) {
            if (strlen($this->value) < $length) {
                $this->errors[$this->name][] = validation_trans('min.string', ['attribute' => $this->name, 'min' => $length]);
            }
        } else {
            if ($this->value < $length) {
                $this->errors[$this->name][] = validation_trans('min.numeric', ['attribute' => $this->name, 'min' => $length]);
            }
        }

        return $this;

    }

    /**
     * Maximum length of the field value
     * ! asdasd
     * @param int $max
     * @return this
     */
    public function max($length)
    {
        $is_number = preg_match('/^(' . $this->patterns['float'] . ')$/u', $this->value);

        if (is_string($this->value) && !$is_number) {
            if (strlen($this->value) > $length) {
                $this->errors[$this->name][] = validation_trans('max.string', ['attribute' => $this->name, 'max' => $length]);
            }

        } else {
            if ($this->value > $length) {
                $this->errors[$this->name][] = validation_trans('max.numeric', ['attribute' => $this->name, 'max' => $length]);
            }
        }
        return $this;

    }

    /**
     * Compare with the value of another field
     *
     * @param mixed $value
     * @return this
     */
    public function equal($value)
    {

        if ($this->value != $value) {
            $this->errors[$this->name][] = validation_trans('equal', ['attribute' => $this->name, 'equal' => $value]);
        }
        return $this;

    }

    /**
     * Check if value has already been taken
     *
     * @param mixed $value
     * @return this
     */
    public function unique($table, $column = null)
    {
        $item = DB::table($table)->where($column, $this->value)->first();

        if (isset($item)) {
            $this->errors[$this->name][] = validation_trans('unique', ['attribute' => $this->name]);
        }
        return $this;

    }

    /**
     * Pattern to be applied to the recognition
     * of the regular expression
     *
     * @param string $name pattern name
     * @return this
     */
    public function pattern($name)
    {
        if ($name == 'array') {

            if (!is_array($this->value)) {
                $this->errors[$this->name][] = validation_trans($name, ['attribute' => $this->name]);
            }

        } else {

            $regex = '/^(' . $this->patterns[$name] . ')$/u';
            if ($this->value != '' && !preg_match($regex, $this->value)) {
                $this->errors[$this->name][] = validation_trans($name, ['attribute' => $this->name]);
            }

        }
        return $this;

    }

    /**
     * Purifies to prevent XSS attacks
     *
     * @param string $string
     * @return $string
     */
    public function purify($string)
    {
        return htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
    }

    /**
     * Validated fields
     *
     * @return boolean
     */
    public function isSuccess()
    {
        if (empty($this->errors)) return true;
    }

    /**
     * Validation errors
     *
     * @return array $this->errors
     */
    public function getErrors()
    {
        if (!$this->isSuccess()) return $this->errors;
    }

    /**
     * View validation result
     *
     * @return booelan|string
     */
    public function result()
    {

        if (!$this->isSuccess()) {

            foreach ($this->getErrors() as $error) {
                echo "$error\n";
            }
            exit;

        } else {
            return true;
        }

    }

    /**
     * heck if the value is an integer
     *
     * @param mixed $value
     * @return boolean
     */
    public static function is_int($value)
    {
        if (filter_var($value, FILTER_VALIDATE_INT)) return true;
    }

    /**
     * Check to see if the value
     * is a float number
     *
     * @param mixed $value
     * @return boolean
     */
    public static function is_float($value)
    {
        if (filter_var($value, FILTER_VALIDATE_FLOAT)) return true;
    }

}