<?php

namespace App\Controllers;


use App\Classes\Validation;
use App\Models\Product;
use App\Models\ProductType;
use App\Models\ProductAttribute;
use Symfony\Component\HttpFoundation\Request;


class ApiController extends Controller
{
    /**
     * Get Product list view with generated twig.
     *
     * @return string The rendered twig template
     */
    public function getProductListView()
    {

        $products = Product::with('product_attributes')->get();
        return $this->twig('product-list.twig', array('products' => $products));
    }

    /**
     * Get new product form with generated twig.
     *
     * @return string The rendered twig template
     */
    public function getProductNewView()
    {

        $product_types = ProductType::all();

        return $this->twig('product-new.twig', array(
            'product_types' => $product_types,
            'product_type' => null)
        );

    }

    /**
     * Save new product of requested data.
     * Validate fields and return if is not success
     *
     * @param Request $request Symfony\Component\HttpFoundation\Request
     *
     * @return string The rendered twig template
     */
    public function saveProductNew(Request $request)
    {
        $request_data   = $request->request->all();
        $validate       = new Validation();
        $form_values    = null;
        $form           = collect();
        $status         = false;
        $error          = '';

        // Get Form values array
        $form_values = arr_key('form', $request_data, $form_values);

        // Validate if array exists
        $validate->name('form')->value($form_values)->pattern('array')->required();

        if (!$validate->isSuccess())
        {
            return $this->json('Form was not received', 400);
        }

        // Build form values
        $request_data = arr_key('form', $request_data, []);
        foreach ($request_data as $key => $item) {
            $form->put($item['name'], $item['value']);
        }
        $form = $form->toArray();

        // Validate
        $validate->name('sku')->value(arr_key('sku', $form, ''))->unique('products', 'sku')->pattern('string')->required();
        $validate->name('name')->value(arr_key('name', $form, ''))->pattern('string')->required();
        $validate->name('price')->value(arr_key('price', $form, ''))->pattern('currency')->min(0)->max(99999999.99)->required();
        $validate->name('type')->value(arr_key('type', $form, ''))->pattern('string')->required();

        switch (arr_key('type', $form)) {
            case 'size':
                $validate->name('size')->value(arr_key('size', $form, ''))->pattern('float')->required();
                break;
            case 'weight':
                $validate->name('weight')->value(arr_key('weight', $form, ''))->pattern('float')->required();
                break;
            case 'dimension':
                $validate->name('height')->value(arr_key('height', $form, ''))->pattern('float')->required();
                $validate->name('width')->value(arr_key('width', $form, ''))->pattern('float')->required();
                $validate->name('length')->value(arr_key('length', $form, ''))->pattern('float')->required();
                break;
        }

        if (!$validate->isSuccess())
        {
            return $this->json($validate->getErrors(), 422);
        }

        $type = ProductType::where('type', $form['type'])->first();

        if(isset($type)) {

            $product = new Product();
            $product->sku = $form['sku'];
            $product->name = $form['name'];
            $product->price = $form['price'];
            $status = $product->save();

            if($status) {

                $attribute = new ProductAttribute();
                $attribute->product_id = $product->id;
                $attribute->product_type_id = $type->id;

                switch (arr_key('type', $form)) {
                    case 'size':
                        $attribute->size = $form['size'];
                        break;
                    case 'weight':
                        $attribute->weight = $form['weight'];
                        break;
                    case 'dimension':
                        $attribute->height = $form['height'];
                        $attribute->width = $form['width'];
                        $attribute->length = $form['length'];
                        break;
                }
                $status = $attribute->save();

            } else {

                $error = 'Product Type not found';
            }

        } else {

            return $this->json($validate->getErrors(), 422);
        }

        return $this->json(['status' => $status, 'error' => $error]);

    }

    /**
     * Get Product type view only contain requested
     * type of product type.
     * Validate fields and return if is not success
     *
     * @param Request $request Symfony\Component\HttpFoundation\Request
     *
     * @return string The rendered twig template
     */
    public function getProductTypeView(Request $request)
    {
        $validate       = new Validation();
        $request_data   = $request->request->all();

        // Validate if array exists

        $validate->name('type')->value(arr_key('type', $request_data, null))->pattern('string')->required();

        if (!$validate->isSuccess())
        {
            return $this->json('Undefined product type', 400);
        }

        $type = arr_key('type', $request_data, null);
        $product_type = ProductType::where('type', $type)->first();

        if(isset($product_type)) {
            return $this->twig('product-types.twig', array(
                'product_type' => $product_type->type
            ));

        }

        return $this->response('Product type not found!', 404);

    }
    /**
     * Delete products with where product id is all requested ids
     * Validate fields and return if is not success
     *
     * @param Request $request Symfony\Component\HttpFoundation\Request
     *
     * @return string The rendered twig template
     */
    public function massDeleteProducts(Request $request)
    {
        $request_data = $request->request->all();
        $validate = new Validation();
        $status = false;
        $error = '';

        // Get Form values array
        $form_values = arr_key('form', $request_data);

        // Validate if array exists
        $validate->name('form')->value($form_values)->pattern('array')->required();

        if (!$validate->isSuccess())
        {
            return $this->json('Form was not received', 400);
        }

        foreach ($request_data['form'] as $input) {
            $prod = Product::where('id', $input['name'])->first();
            if(isset($prod)) {
                $status = $prod->delete();
            }
        }

        if($status == false) {
            $error = 'Cant remove some of requested products';
        }

        return $this->json(['status' => $status, 'error' => $error]);
    }
}