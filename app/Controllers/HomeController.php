<?php

namespace App\Controllers;

use App\Models\Test;
use App\Models\User;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends Controller
{

    /**
     * Redirect any unregistred route.
     *
     * @param object $request    The request data
     * @param array   $args  The request parameters
     *
     * @return string The rendered template
     */
    public function all(Request $request)
    {
        $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https'?'https':'http';
        $data =  $protocol.'://'.$_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'];
        return $this->redirect($data.'/product/list');
    }

}