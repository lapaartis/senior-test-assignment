<?php

namespace App\Controllers;

use Symfony\Component\HttpFoundation\Request;
use App\Models\Product;

class ProductController extends Controller
{

    /**
     * Return base template for spa
     *
     * @return string The rendered twig template
     */
    public function index()
    {
        $products = Product::with('product_attributes')->get();

        return $this->twig('products.twig', [
            'products' => $products
        ]);
    }

}