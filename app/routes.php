<?php


use App\Classes\Router;

$route = new Router();

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application.
|
*/


$route->get('/product/list', 'App\Controllers\ProductController::index', 'product-list');
$route->get('/product/new', 'App\Controllers\ProductController::index', 'product-new');

$route->post('/api/get/product-list-view', 'App\Controllers\ApiController::getProductListView');
$route->post('/api/get/product-new-view', 'App\Controllers\ApiController::getProductNewView');

$route->post('/api/get/product-type-view', 'App\Controllers\ApiController::getProductTypeView');

$route->post('/api/save/product-new', 'App\Controllers\ApiController::saveProductNew');
$route->delete('/api/products/mass-delete', 'App\Controllers\ApiController::massDeleteProducts');

//// Catch Any unregistred route and redirect to /product/list
$route->get('/', 'App\Controllers\HomeController::all');
//$route->get('/{req}', 'App\Controllers\HomeController::all');


/*
|--------------------------------------------------------------------------
| Get routes and return for execute
|--------------------------------------------------------------------------
*/

return $route->getRoutes();

