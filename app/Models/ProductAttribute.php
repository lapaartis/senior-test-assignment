<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductAttribute extends Model
{
    protected $table = 'product_attributes';

    /**
     * Get the product that owns the product attribute.
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Products', 'product_id', 'id');
    }

    /**
     * Get the product type that owns the product attribute.
     */
    public function product_type()
    {
        return $this->belongsTo('App\Models\ProductType','product_type_id', 'id');
    }
}