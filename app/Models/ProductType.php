<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
    protected $table = 'product_types';

    /**
     * Get all of the product attributes for the product type.
     */
    public function product_attributes()
    {
        return $this->hasMany('App\Models\ProductAttribute', 'product_type_id', 'id');
    }
}