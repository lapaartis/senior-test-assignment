<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    /**
     * Get all of the product attributes for the product.
     */
    public function product_attributes()
    {
        return $this->hasOne('App\Models\ProductAttribute', 'product_id', 'id');
    }

    protected static function boot() {
        parent::boot();

        static::creating(function($node) {
        });
        static::deleting(function($node) {
            $attr = ProductAttribute::where('product_id', $node->id)->first();
            if(isset($attr)) {
                $attr->delete();
            }
        });
    }
}