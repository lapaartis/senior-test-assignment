<?php

use App\Classes\App;


/*
|--------------------------------------------------------------------------
| Run the ENV file reader
|--------------------------------------------------------------------------
|
| Require to use $_ENV parameters in out Application
|
*/

josegonzalez\Dotenv\Loader::load([
    'filepath' => __DIR__ . '/../../.env',
    'toEnv'    => true
]);

/*
|--------------------------------------------------------------------------
| Init App config
|--------------------------------------------------------------------------
|
| Require to use App config params like $app_config::get('name').
|
*/

$app_config = new App(include __DIR__ . '/../../config/app.php');

require __DIR__ . '/db.php';

require __DIR__ . '/routing.php';


