<?php

if (! function_exists('base_path')) {

    /**
     * Get Project Base Path
     *
     * @param  string  $path
     * @return string
     */
    function base_path($path = "") {
        return realpath(__DIR__ . '/../../') . DIRECTORY_SEPARATOR . $path;
    }
}

if (! function_exists('trans')) {

    /**
     * Translate the given message.
     *
     * @param  string  $key
     * @param  array   $replace
     * @return string
     */
    function trans($key = null, $replace = [])
    {
        $validation_trans = require __DIR__.'/../../resources/lang/validation.php';

        if(is_null($key)) {
            return 'Translation key is null';
        }

        $trans_string = getNestedVar($validation_trans, $key);

        if(is_null($trans_string)) {
            return $key;
        }

        foreach ($replace as $place => $name) {
            $trans_string = str_replace(":{$place}", $name, $trans_string);
        }

        return $trans_string;
    }
}

if (! function_exists('validation_trans')) {

    /**
     * Validation translate the given message.
     *
     * @param  string  $key
     * @param  array   $replace
     * @return string
     */
    function validation_trans($key = null, $replace = [])
    {
        $trans = require __DIR__.'/../../resources/lang/validation.php';

        if(is_null($key)) {
            return 'Translation key is null';
        }

        $trans_string = getNestedVar($trans, $key);

        if(is_null($trans_string)) {
            return $key;
        }

        foreach ($replace as $place => $name) {
            $trans_name = array_key_exists(strval($name), $trans['attributes']) ? $trans['attributes'][strval($name)] : strval($name);
            $trans_string = str_replace(":{$place}", $trans_name, $trans_string);
        }

        return $trans_string;
    }
}

if (! function_exists('getNestedVar')) {

    /**
     * Get string with dots from nasted array
     *
     * @param  array $context
     * @param  string $name
     * @return string
     */
    function getNestedVar(&$context, $name)
    {
        $pieces = explode('.', $name);

        foreach ($pieces as $piece) {
            if (!is_array($context) || !array_key_exists(strval($piece), $context)) {
                // error occurred
                return null;
            }
            $context = &$context[$piece];
        }

        return $context;
    }
}

if (! function_exists('arr_key')) {

    /**
     * Check if array has a key
     *
     * @param  string $key
     * @param  $array $array
     * @return string|null
     */
    function arr_key($key, array $array, $false = null)
    {
        if(!array_key_exists(strval($key), $array)) {
            return $false;
        }

        return $array[$key];
    }
}