<?php

use Symfony\Component\Debug\Debug;
use Symfony\Component\Debug\DebugClassLoader;
use Symfony\Component\Debug\ErrorHandler;
use Symfony\Component\Debug\ExceptionHandler;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\HttpKernel\EventListener\RouterListener;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;

/*
|--------------------------------------------------------------------------
| Check request and execute functions
|--------------------------------------------------------------------------
*/

// Enable app symfony debug.
if(arr_key('APP_DEBUG', $_ENV) ? $_ENV['APP_DEBUG'] : false) {
    DebugClassLoader::enable();
    ExceptionHandler::register();
    ErrorHandler::register();
    Debug::enable();
}

// Get routes form external file
$routes = include base_path('app/routes.php');


// Get a new request with values from PHP's super globals.
$request = Request::createFromGlobals();


/*
|--------------------------------------------------------------------------
| Match routes
|--------------------------------------------------------------------------
*/

$context = new RequestContext();
$context->fromRequest($request);
$matcher = new UrlMatcher($routes, $context);

try {
    $parameters = $matcher->match($request->getPathInfo());
} catch (\Exception $e) {
    $response = new Response($e->getMessage(),
        Response::HTTP_INTERNAL_SERVER_ERROR,
        array('content-type' => 'text/html'));
    $response->send();
    exit;
}

// Add [_controller] and other parameters
$request->attributes->add($parameters);

/*
|--------------------------------------------------------------------------
| Run Event dispatcher
|--------------------------------------------------------------------------
*/

$dispatcher = new EventDispatcher();
$dispatcher->addSubscriber(new RouterListener($matcher, new RequestStack()));

$controllerResolver = new ControllerResolver();
$argumentResolver = new ArgumentResolver();

$controller = $controllerResolver->getController($request);
$arguments = $argumentResolver->getArguments($request, $controller);

$kernel = new HttpKernel($dispatcher, $controllerResolver, new RequestStack(), $argumentResolver);

$response = $kernel->handle($request, HttpKernelInterface::SUB_REQUEST);

/*
|--------------------------------------------------------------------------
| Response back app respones
|--------------------------------------------------------------------------
*/

$response->send();

$kernel->terminate($request, $response);


/*
 * OLD routing - is working too.
 * */

//use Symfony\Component\HttpFoundation\Request;
//use Symfony\Component\HttpFoundation\Response;
//use Symfony\Component\Routing\Matcher\UrlMatcher;
//use Symfony\Component\Routing\RequestContext;
//use Symfony\Component\Debug\Debug;
//use Symfony\Component\Debug\ErrorHandler;
//use Symfony\Component\Debug\ExceptionHandler;
//use Symfony\Component\Debug\DebugClassLoader;
//
//if(arr_key('APP_DEBUG', $_ENV) ? $_ENV['APP_DEBUG'] : false) {
//    DebugClassLoader::enable();
//    ExceptionHandler::register();
//    ErrorHandler::register();
//    Debug::enable();
//}
//
//$request = Request::createFromGlobals();
//
//$routes = include base_path('app/routes.php');
//
//$context = new RequestContext();
//
//$context->fromRequest($request);
//
//$matcher = new UrlMatcher($routes, $context);
//
//$parameters = $matcher->matchRequest($request);
//
//$request_params = collect();
//foreach ($parameters as $key => $item) {
//    if(!($key == '_controller' || $key == '_route')) {
//        $request_params->put($key, $item);
//    }
//}
//
//list($controllerClassName, $action) = explode('::',$parameters['_controller']);
//$controller = new $controllerClassName();
//$response = $controller->{$action}($request, $request_params);
//
//if(is_string($response)) {
//    $response = new Response($response);
//}
//
//$response->send();

