<?php

use Illuminate\Database\Capsule\Manager;

Manager::schema()->create('product_attributes', function ($table) {
    $table->charset = 'utf8';
    $table->collation = 'utf8_unicode_ci';
    $table->engine = 'InnoDB';

    $table->increments('id');
    $table->integer('product_id')->unsigned();
    $table->integer('product_type_id')->unsigned();
    $table->integer('size')->default('0');
    $table->integer('height')->default('0');
    $table->integer('width')->default('0');
    $table->integer('length')->default('0');
    $table->integer('weight')->default('0');
    $table->timestamps();

    // FOREIGN
    $table->foreign('product_id')->references('id')->on('products');
    $table->foreign('product_type_id')->references('id')->on('product_types');

    // INDEXES
    $table->index(['product_id', 'product_type_id']);
    $table->index(['product_type_id']);
});
