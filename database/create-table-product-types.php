<?php

use Illuminate\Database\Capsule\Manager;

Manager::schema()->create('product_types', function ($table) {
    $table->charset = 'utf8';
    $table->collation = 'utf8_unicode_ci';
    $table->engine = 'InnoDB';

    $table->increments('id');
    $table->string('name')->nullable();
    $table->string('type')->nullable();
    $table->timestamps();

    $table->index(['name']);
});
