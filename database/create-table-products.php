<?php

use Illuminate\Database\Capsule\Manager;

Manager::schema()->create('products', function ($table) {
    $table->charset = 'utf8';
    $table->collation = 'utf8_unicode_ci';
    $table->engine = 'InnoDB';

    $table->increments('id');
    $table->string('sku')->unique();
    $table->string('name')->default('');
    $table->decimal('price', 8, 2)->default('0');
    $table->timestamps();

    // INDEXES
    $table->index(['sku', 'name']);
    $table->index(['name']);
    $table->index(['price']);
});
