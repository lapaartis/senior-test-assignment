<?php

use App\Models\ProductType;


class Type
{
    public $name;
    public $type;

    function set($name, $type) {
        $this->name = $name;
        $this->type = $type;
        return $this;
    }
}


$type_list = collect();
$type_list->push((new Type())->set('DVD-disc', 'size'));
$type_list->push((new Type())->set('Book', 'weight'));
$type_list->push((new Type())->set('Furniture', 'dimension'));


foreach ($type_list as $type) {
    $new = new ProductType();
    $new->name = $type->name;
    $new->type = $type->type;
    $new->save();
}