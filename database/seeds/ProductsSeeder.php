<?php

use App\Models\Product;
use App\Models\ProductType;
use App\Models\ProductAttribute;

$products = collect();
$product_types = ProductType::all();

// Seed Size type Products
$product_list = ['Acme Disc','Kingston USB','A-DATA USB','Samsung SSD'];
for($i = 0; $i < count($product_list); $i++) {
    $products->push(['sku' => 'JVC'.rand(100000,999999),'name' => $product_list[$i],'price' => rand(10, 999) / 10, 'type' => 'size']);
}

// Seed Dimension type Products
$product_list = ['Chair','Table','Stool','Bench'];
for($i = 0; $i < count($product_list); $i++) {
    $products->push(['sku' => 'TR'.rand(100000,999999),'name' => $product_list[$i],'price' => rand(10, 999) / 10, 'type' => 'dimension']);
}

// Seed Weight type Products
$product_list = ['Baskin Robbins','Betty Crocker','Cadbury Cake Bars','Cadbury Highlights'];
for($i = 0; $i < count($product_list); $i++) {
    $products->push(['sku' => 'GGWP'.rand(1000,9999),'name' => $product_list[$i],'price' => rand(10, 999) / 10, 'type' => 'weight']);
}

foreach ($products as $product) {
    $new = new Product();
    $new->sku = $product['sku'];
    $new->name = $product['name'];
    $new->price = $product['price'];

    $new_product_types = $product_types->where('type', $product['type']);

    if($new_product_types->count() > 0) {
        $new->save();
        foreach ($new_product_types as $new_product_type) {
            $new_attr = new ProductAttribute();
            $new_attr->product_id = $new->id;
            $new_attr->product_type_id = $new_product_type->id;
            if($new_product_type->type == 'size') {
                $new_attr->size = rand(10, 999);
            } else if($new_product_type->type == 'dimension') {
                $new_attr->height = rand(10, 100);
                $new_attr->width = rand(10, 100);
                $new_attr->length = rand(10, 100);
            } else if($new_product_type->type == 'weight') {
                $new_attr->weight = rand(10, 999);
            }
            $new_attr->save();
        }
    }
    else {
        echo 'Product type not found!';
    }
}