# Scandiweb tests

Project background:

* XAMPP v3.2.2
* PHP 7.2.11 
* Composer version 1.7.2 
* MySql 10.1.36-MariaDB

Composer packages:
* illuminate/database": "^5.7",
* "illuminate/events": "^5.7",
* "josegonzalez/dotenv": "^3.2",
* "symfony/routing": "^4.1",
* "symfony/http-foundation": "^4.1",
* "symfony/http-kernel": "^4.1"
* "twig/twig": "^2.5",
* "symfony/debug": "^4.1"
   
What to do:
* Prepare the project environment. I use XAMPP v3.2.2
* Create a database for the project and its user.
```sh
mysql -u root -p
CREATE DATABASE database-name;
GRANT ALL PRIVILEGES ON database-name.* TO 'user'@'localhost' IDENTIFIED BY 'your-password;
```
* Git clone repository from https://bitbucket.org/lapaartis/senior-test-assignment/src/master/
* Run the command: composer update 
* Copy and rename .env.example to .env and define database credentials
    * DB_DATABASE
    * DB_USERNAME
    * DB_PASSWORD
* Launch some kind of migration with commands from root path (C:/path/to/project/):
```sh
$ php create-tables.php
$ php seed-tables.php
```
* If the .env database credentials added successfully, then in command line display messages like:
```sh
[aleaflv@server26 scandiweb.kods.lv]$ php create-tables.php
string(36) "Database tables created Succesfully!"
```
* If you need to delete database data and tables with drop-table.php you can do this
```sh
$ php drop-tables.php
$ php create-tables.php
$ php seed-tables.php
```
* The launch of the project starts from the /public folder.
    * To run from the command line:
```sh
$ cd /public
$ php `php -S localhost:8080`
```
* The project has been successfully implemented. 
 