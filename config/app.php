<?php

return [

    'slim' => [
        'settings' => [
            'displayErrorDetails' => $_ENV['APP_DEBUG']
        ],
        'debug' => $_ENV['APP_DEBUG'],
        'mode' => $_ENV['APP_ENV']
    ],

    'db' => [
        'driver'    => 'mysql',
        'host'      => $_ENV['DB_HOST'],
        'database'  => $_ENV['DB_DATABASE'],
        'username'  => $_ENV['DB_USERNAME'],
        'password'  => $_ENV['DB_PASSWORD'],
        'port'      => $_ENV['DB_PORT'],
        'charset'   => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix'    => '',
    ]
];