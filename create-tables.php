<?php

require __DIR__.'/vendor/autoload.php';
require 'bootstrap/db.php';

include __DIR__.'/database/create-table-product-types.php';
include __DIR__.'/database/create-table-products.php';
include __DIR__.'/database/create-table-product-attributes.php';

var_dump('Database tables created Succesfully!');