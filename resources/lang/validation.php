<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class.
    |
    */

    'array' => 'The :attribute must be an array.',
    'currency' => 'The :attribute must be a currency.',
    'equal' => 'The :attribute must be equal to :equal.',
    'integer' => 'The :attribute must be an integer.',
    'float' => 'The :attribute must be an float.',
    'less' => [
        'numeric' => 'The :attribute must be at least :min.',
        'string' => 'The :attribute must be at least :min characters.',
    ],
    'min' => [
        'numeric' => 'The :attribute must be at least :min.',
        'string' => 'The :attribute must be at least :min characters.',
    ],
    'max' => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'string' => 'The :attribute may not be greater than :max characters.',
    ],
    'required' => 'The :attribute field is required.',
    'string' => 'The :attribute must be a string.',
    'unique' => 'The :attribute has already been taken.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | Swap attribute place-holders with something more reader friendly
    | such as E-Mail Address instead of "email".
    |
    */

    'attributes' => [
//        'some_name' => 'Custom name',
    ],
];