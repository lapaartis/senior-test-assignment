$spa.setAppId('app');

// App Routes
$spa.addRoute('/product/list', 'product_list', 'Product List');
$spa.addRoute('/product/new', 'product_new', 'Product Add');

// App Controllers
$spa.addController('product_list', function () {

    var app = $spa.app_id;
    $(app + ' #page_name').html($spa.currentRoute.t);

    var loadProductsListView = function () {
        $('#content').html($spa.getLoading());
        $.ajax({
            url: window.location.origin + '/api/get/product-list-view',
            method: 'POST',
            success: function (data) {
                $('#content').html(data);
            }
        });
    };

    var setProductClick = function () {
        $(app + ' #content')
            .off('click', '.product-item')
            .on('click', '.product-item', function (event) {
                // event.preventDefault();

                if (event.target.type != 'checkbox') {
                    $input = $(this).find('input[type="checkbox"]');
                    $input.prop('checked', !$input.prop('checked'));
                }

                if ($input.prop('checked')) {
                    $(this).addClass('selected');
                } else {
                    $(this).removeClass('selected');
                }

                var selected = [];
                $(app + ' #content input:checked').each(function () {
                    selected.push($(this).attr('name'));
                });
                console.log(selected);
            });
    };
    var setProductActionsChange = function () {
        $(app + ' #product_actions').val('');
    };
    var setProductActionsBtnClick = function () {


        $(app)
            .off('click', '#product_action_btn')
            .on('click', '#product_action_btn', function (event) {
                var $action_select = $(app + ' #product_actions');

                if ($action_select.val() == 'delete') {
                    var form = $('form#product_list_form').serializeArray();
                    console.log(form.length);
                    if(form.length > 0) {
                        $.ajax({
                            url: window.location.origin + '/api/products/mass-delete',
                            method: 'DELETE',
                            data: {form: form},
                            success: function (data) {
                                if(data.status == true) {
                                    // $spa.changePathByController('product_list');
                                    loadProductsListView();
                                } else {
                                    alert(data.error);
                                }
                            },
                            error: function (data) {
                                switch(data.status) {
                                    case 422:
                                        console.log(data.responseJSON);
                                        // validationError('#form_product_add', data.responseJSON);
                                        break;
                                    default:
                                        alert(`${data.statusText}: ${data.responseJSON}`);
                                }
                            }
                        });
                    } else {
                        alert('0 Products are selected!');
                        // $('#dialog').text('0 Products are selected!');
                    }
                } else {
                    $action_select.addClass('shake');
                    setTimeout(function () {
                        $action_select.removeClass('shake');
                    }, 1000);
                }
            });
    };

    setProductActionsBtnClick();
    setProductActionsChange();
    loadProductsListView();
    setProductClick();
});

$spa.addController('product_new', function () {
    var app = $spa.app_id;
    $(app + ' #page_name').html($spa.currentRoute.t);
    $(app + ' #content').html('');

    var loadProductsAddView = function () {
        $('#content').html($spa.getLoading());
        $.ajax({
            url: window.location.origin + '/api/get/product-new-view',
            method: 'POST',
            success: function (data) {
                $('#content').html(data);
            }
        });
    };

    var loadProductsAddTypeView = function (data) {

        $(app + ' #type_switcher_view').html($spa.getLoading());

        $.ajax({
            url: window.location.origin + '/api/get/product-type-view',
            method: 'POST',
            data: data,
            success: function (data) {
                $(app + ' #type_switcher_view').html(data);
            }
        });
    };

    var setTypeSwitcherChange = function () {
        $(app)
            .off('change', '#type_switcher')
            .on('change', '#type_switcher', function (event) {
                // event.preventDefault();
                if (event.target.value != '') {
                    var data = {
                        type: event.target.value
                    };
                    loadProductsAddTypeView(data);
                } else {
                    $(app + ' #type_switcher_view').html('');
                }

            });
    };

    var setProductSaveClick = function () {
        $(app)
            .off('click', '#product_add_save')
            .on('click', '#product_add_save', function (event) {
                // event.preventDefault();
                var form = $('form#form_product_add').serializeArray();
                $.ajax({
                    url: window.location.origin + '/api/save/product-new',
                    method: 'POST',
                    data: {form: form},
                    success: function (data) {
                        if (data.status == true) {
                            $spa.changePathByController('product_list');
                        } else {
                            alert(data.error);
                        }
                    },
                    error: function (data) {
                        switch (data.status) {
                            case 422:
                                validationError('#form_product_add', data.responseJSON);
                                break;
                            default:
                                alert(`${data.statusText}: ${data.responseJSON.toString()}`);
                        }
                    }
                });
            });
    };

    var validationError = function (find, errors) {
        $(find + ' [name]').each(function (idx, el) {
            var feedback = el.nextElementSibling;

            if (feedback != undefined && feedback.classList.contains('feedback')) {
                feedback.parentNode.removeChild(feedback);
            }

            if (errors[el.name] != undefined) {

                var ul = document.createElement("ul");
                ul.classList.add('feedback');

                $.each(errors[el.name], function (index, value) {
                    var li = document.createElement('li');
                    li.append(document.createTextNode(value));
                    ul.append(li);
                });

                insertAfter(ul, el);
            }

        });
    };

    function insertAfter(newNode, referenceNode) {
        referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
    }

    loadProductsAddView();
    setTypeSwitcherChange();
    setProductSaveClick();
});


