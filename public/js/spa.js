var Route = function (p, c, t) {
    this.p = p;
    this.c = c;
    this.t = t;
};
var Controller = function (n, a) {
    this.n = n;
    this.f = a;
};

var $spa = new (function () {
    this.app_id = null;
    this.routes = [];
    this.controllers = [];
    this.currentRoute = null;
    this.currentController = null;

    // Set Application ID
    // @param String n:name
    // @param String c:controller
    this.setAppId = function (id) {
        this.app_id = '#' + id;
    };

    // Add SPA Route
    // @param String n:name
    // @param String c:controller
    this.addRoute = function (n, c, t) {
        this.routes.push(new Route(n, c, t));
    };

    // Find defined route or undefined
    // @param String n:name
    // Return new Route|undefined
    this.findRoute = function (n) {
        return this.routes.find(find => find.p === n);
    };

    // Find defined route by controller name or undefined
    // @param String n:name
    // Return new Route|undefined
    this.findRouteByController = function (n) {
        return this.routes.find(find => find.c === n);
    };

    // Add SPA Controller
    // @param String n:name
    // @param Function f:function
    this.addController = function (n, f) {
        this.controllers.push(new Controller(n, f));
    };

    // Find defined controller or undefined
    // @param String n:name
    // Return new Controller|undefined
    this.findController = function (n) {
        return this.controllers.find(find => find.n === n);
    };

    this.changePath = function (pathname) {
        window.history.pushState("data","Title",pathname);
        var route = $spa.findRoute(pathname);
        if(route != undefined) {
            document.title = route.p === undefined ? document.title : route.t;
            $spa.execute(window.location.pathname);
        }
    };
    this.changePathByController = function (name) {

        var route = $spa.findRouteByController(name);
        if(route != undefined) {
            window.history.pushState("data","Title",route.p);
            document.title = route.t === undefined ? document.title : route.t;
            $spa.execute(window.location.pathname);
        } else {
            console.error('Undefined Controller');
        }

    };

    this.spaShow = function (c_name) {
        $('[spa-show]').each(function( idx, el ) {
            if(el.getAttribute('spa-show') == c_name) {
                el.style.display = "block";
            } else {
                el.style.display = "none";
            }
        });
    };

    // Execute SPA public
    // @param String pathname
    this.execute = function (pathname) {
        this.currentRoute = this.findRoute(pathname);
        document.title = this.currentRoute.t === undefined ? document.title : this.currentRoute.t;
        if (this.currentRoute !== undefined) {
            this.currentController = this.findController(this.currentRoute.c);
            if (this.currentController !== undefined) {
                $spa.spaShow(this.currentController.n);
                this.currentController.f(); // Run controller function()
            } else {
                console.error(`Controller "${this.currentRoute.c}" not found!`);
            }
        } else {
            console.error(`Route "${pathname}" not found!`);
        }
    };

    this.getLoading = function(){
        return '<div class="lds-wrap"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>';
    };

    var ready = function(callback){
        // in case the document is already rendered
        if (document.readyState!='loading') callback();
        // modern browsers
        else if (document.addEventListener) document.addEventListener('DOMContentLoaded', callback);
        // IE <= 8
        else document.attachEvent('onreadystatechange', function(){
            if (document.readyState=='complete') callback();
        });
    };

    window.onpopstate = function(event) {
        $spa.execute(window.location.pathname);
    };

    ready(function(){
        $spa.execute(window.location.pathname);
    });
});

(function(history){
    var pushState = history.pushState;
    history.pushState = function(state) {
        if (typeof history.onpushstate == "function") {
            history.onpushstate({state: state});
            console.log(state);
        }
        console.log(window.location.pathname);
        // $spa.execute();
        // ... whatever else you want to do
        // maybe call onhashchange e.handler
        return pushState.apply(history, arguments);
    }
})(window.history);